from os import listdir, fsdecode, fsencode, mkdir, path
import shutil
import argparse
import sys
from subprocess import run

def PARAMETERS():
    
    # Define parameters which are used to build directory paths in each of the programs
    
    # what to process?
    cameras = [1,2,3]
    sequences = [17]
    frame_range = '0-999'       # of the form '{firstframe}-{lastframe}'
    
    # data from when?           e.g. E:\rawdata\FW02\230208\F1\rawHR\
    project = 'FW02'            # must be same as in directory structure
    date = '230218'             # yymmdd
    flight = 'F1'               # must be same as in directory structure
    
    return cameras, sequences, frame_range, project, date, flight
    
    
def main():

    # grab parameters to pass into each function
    cameras, sequences, frame_range, project, date, flight = PARAMETERS()
    
    SEQdrive_order = ['I','J','H']
    
    # process the first sequence file for cameras 1,2,3 then the second sequence, third, so on...
    for sequence_ in sequences:
        sequence = str(sequence_).zfill(3)
        for camera_ in cameras:
            
            if camera_ == 1:
                SEQdrive = SEQdrive_order[0]
            elif camera_ == 2:
                SEQdrive = SEQdrive_order[1]
            elif camera_ == 3:  
                SEQdrive = SEQdrive_order[2]
            
            camera = str(camera_)
            
            # Build common directories
            rawHR_dir = 'E:\\rawdata\\'+project+'\\'+date+'\\'+flight+'\\rawHR\\'
            raw_dir = rawHR_dir + 'C'+camera+'\\raw\\'
            work_dir =  raw_dir + 'Working'
            jpg_dir = rawHR_dir + 'C'+ camera +'\\jpg\\'
            
            # Let the user know where they are up to
            print(f'\n---------------------------------')
            print(f'----- Sequence {sequence} Camera {camera} -----')
            print(f'---------------------------------')
            
            # Execute programs
            print(f'\n-----Export_Seq2Raw-----')
            Export_Seq2Raw(sequence, camera, frame_range, project, date, flight, rawHR_dir, raw_dir, SEQdrive)
            print(f'\n-----Rename_Raw2Working-----')
            Rename_Raw2Working(sequence, camera, frame_range, project, date, flight, work_dir, raw_dir)
            print(f'\n-----Debayer_Working2Jpg-----')
            Debayer_Working2Jpg(sequence, camera, frame_range, project, date, flight, raw_dir, work_dir, jpg_dir)

    
    
def Export_Seq2Raw(sequence, camera, frame_range, project, date, flight, rawHR_dir, raw_dir, SEQdrive):
    
    # Locate the sequence file
    SEQpath = SEQdrive+':HR_'+date+'_'+flight+'_C'+camera+'\\'+'HR_'+date+'_'+flight+'_C'+camera+'_'+sequence+'.seq'

    
    # Export all images for the current sequence and camera
    print('Exporting from sequence file to raw folder...')
    # eg: .\hrimgexport_basetest.exe -s E:\rawdata\FW02\230208\F1\rawHR\HR_230208_F1_C1_001.seq 	-f 0-999 	-o E:\rawdata\FW02\230208\F1\rawHR\C1\raw
    run([".\hrimgexport_basetest.exe","-s",SEQpath,"-f",frame_range,"-o",raw_dir])
    
    
def Rename_Raw2Working(sequence, camera, frame_range, project, date, flight, work_dir, raw_dir):
    
    # Renames raw images from frameXX.jpg to sensible name and places inside Working directory
    # Example of oldname and newname
        # oldname: E:\rawdata\FW02\230118\F2\rawHR\C1\jpg\frame12.jpg
        # newname: E:\rawdata\FW02\230118\F2\rawHR\C1\jpg\HR_230118_F2_C1_004_0012.jpg
        
    # JUSTIFICATION for using a Working directory
        # A Working directory is necessary because hrdebayer.exe will debay everything in the input folder, so we need a subdirectory to not keep debayering a compounding number of images (most of which are the same).
        # The script was not written to debayer the raw folder only once, because any attempt to go back and debayer more imagery later would be a headache. 
        # Also splitting the debayering into cameras and sequence files means you can sooner see a set of output jpg's than if you were to do one large batch.

    # Make working directory
    if not path.exists(work_dir):        
        mkdir(work_dir)
    
    # For each image in raw folder
    print('Renaming as below, and placing inside Working folder')
    for image in listdir(fsencode(raw_dir)):
        image_name = fsdecode(image)
        
        # Skip if the 'image' is actually the Working directory
        if image_name == 'Working':
            continue

        # Skip images with the correct name already
        if len(image_name) > 20:
            continue
        
        # Grab the frame number XX from the name frameXX.jpg (5 chars from the start, to 4 chars from the end)
        frame_number = image_name[5:-4]
        
        #!!!!!!!!!!!! BANDAID FIX, NAME THE FRAME NUMBERS 1 LOWER TO CORRECTLY HAVE PIXREF CORRECTLY PLACE THEM !!!!!!!!
        #print("\n!!!\n!!!\nWRITING THE FRAME NUMBER SUBTRACTED BY 1 TO CORRECT FOR IMPROPER INDEXING!!!\n!!!\n")
        #frame_number = str(int(frame_number)-1)
        
        
        # Build variables for rename and move
        old_path = raw_dir + image_name
        new_name = 'HR_'+ date +'_'+ flight +'_C'+ camera +'_'+ sequence +'_'+ frame_number.zfill(4) +'.raw'
        new_path = path.join(work_dir, new_name) #work_dir + '\\' + new_name
        
        # Let the user check the names
        print(image_name+' --> '+new_name)
        
        # Rename the files and move them to the Working directory
        shutil.move(old_path, new_path)


def Debayer_Working2Jpg(sequence, camera, frame_range, project, date, flight, raw_dir, work_dir, jpg_dir):
    
    # Debayers raw images in the Working directory (outputting to jpg folder)
    # Stores the raw images by moving back to raw folder. 
    # Deletes working directory.

    # Check working directory
    if not path.exists(work_dir):
        print('Working directory doesnt exist, but was supposed to after RenameRaw.py. This is a problem.')
        sys.exit(0)
      
    # Debayer everything in working directory to jpg directory
    print('Debayering to jpg folder...') 
    # eg: .\hrdebayer.exe -i E:\rawdata\FW02\230118\F2\rawHR\C1\raw 	-o E:\rawdata\FW02\230118\F2\rawHR\C1\jpg 	-r 1 	-j
    run([".\hrdebayer.exe", "-i", work_dir, "-o", jpg_dir, "-r", "1", "-j"])
    
    print('Moving below from Working directory --> raw for storage')
    
    # For each image in Working folder
    for image in listdir(fsencode(work_dir)):
        
        # Get image name
        image_name = fsdecode(image)
        print(image_name)
        
        # Set input path
        work_path = work_dir+'\\'+image_name
        
        # Set output path
        raw_path = raw_dir+image_name
        
        # Move from Working to raw to store them
        shutil.move(work_path, raw_path)
    
    # Delete Working directory
    print(f"\nDeleting Camera {camera} working directory")
    shutil.rmtree(work_dir)


if __name__ == '__main__':
    main()
    exit()
