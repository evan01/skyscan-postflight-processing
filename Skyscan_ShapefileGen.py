from os import remove, path, listdir, fsdecode, fsencode
from time import perf_counter
from subprocess import run
from osgeo import gdal, ogr
import geopandas as gpd
from numpy import nonzero
from tqdm import tqdm
from Skyscan_Config import setParams


def extract2shapefile(tif_indir, Shapefile_path, Detections_feat, Detections_layer, driver, point):

    tif_indir_encode = fsencode(tif_indir)
    print("!!!WARNING!!! debug_mode set to TRUE, creating a shapefile in Intermediary_Shapefiles for every frame")
    print("debug_mode is slower, if speed is desired please disable")
    
    print("Recording detection coordinates (pixels of value 255) in every tif...")
    
    for tif in tqdm(listdir(tif_indir_encode)):
        
        # Open only tifs
        tif_decode = fsdecode(tif)
        if not tif_decode.endswith('.tif'):
            continue

        # Read raster, band1, and create array from band1
        raster = gdal.Open(tif_indir + "\\" + tif_decode)
        band1 = raster.GetRasterBand(1)
        (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = raster.GetGeoTransform()
        array = band1.ReadAsArray().astype(int)
        
        # This evaluation makes an x/y index array containing every pixel cell with the fixed value, 255.
        (y_indices, x_indices) = nonzero(array == 255)

        # DEBUG: index arrays
        #print (f"x_indices, y_indices: {x_indices}, {y_indices}")
        #print(f"Number of indices: {len(x_indices)}")

        # Initialize shapefile
        Shape_ds = driver.CreateDataSource(Shapefile_path + tif_decode.replace(".tif", ".shp"))
        Shape_layer = Shape_ds.CreateLayer('ogr_pts', None, ogr.wkbPoint)
        Shape_def = Shape_layer.GetLayerDefn()
        Shape_feat = ogr.Feature(Shape_def)
        
        # Create coordinates from numpy array and add to shapefile
        i = 0
        for x_index in x_indices:

            x = upper_left_x + x_indices[i] * x_size + (x_size / 2) #add half the cell size to centre the point
            y = upper_left_y + y_indices[i] * y_size + (y_size / 2)
            
            # DEBUG: coordinates
            #print (f"FOR X INDEX: {x_index},  Coords (x,y): {str(x)}, {str(y)}")
            #print(f"x_indices:{x_indices[i]}, x_size:{x_size}, upper_left_x:{upper_left_x}")
            #print(f"y_indices:{y_indices[i]}, y_size:{y_size}, upper_left_y:{upper_left_y}")

            point.SetPoint(0, x, y)
            Shape_feat.SetGeometry(point)
            Shape_layer.CreateFeature(Shape_feat)
            
            i += 1
            # print(f"A coordinate pair was created for each of the {i} indices!")    # This line will slow down processing
        
        # Clone shapefile data to Detections
        for feature in Shape_layer:
             Detections_feat.SetGeometry(feature.GetGeometryRef().Clone())
             Detections_layer.CreateFeature(Detections_feat)
        Detections_layer.SyncToDisk()
    print("Finished cloning points from Intermediary_Shapefiles to Detections")

def extract2Detections(tif_indir, Detections_feat, Detections_layer, point):

    tif_indir_encode = fsencode(tif_indir)

    print("Recording detection coordinates (pixels of value 255) in every tif...")

    for tif in tqdm(listdir(tif_indir_encode)):
        
        # Open only tifs
        tif_decode = fsdecode(tif)
        if not tif_decode.endswith('.tif'):
            continue
        #print()
        #print(tif_decode)

        # Read raster, band1, and create array from band1
        raster = gdal.Open(tif_indir + "\\" + tif_decode)
        band1 = raster.GetRasterBand(1)
        (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = raster.GetGeoTransform()
        array = band1.ReadAsArray().astype(int)
        
        # This evaluation makes a numpy x/y index array containing every pixel cell with the fixed value, 255.
        (y_indices, x_indices) = nonzero(array == 255)
        
        # DEBUG: index arrays
        #print (f"x_indices, y_indices: {x_indices}, {y_indices}")
        #print(f"Number of indices: {len(x_indices)}")

        # Create coordinates from numpy array and add to shapefile
        i = 0
        for x_index in x_indices:
            
            x = upper_left_x + x_indices[i] * x_size + (x_size / 2) #add half the cell size to centre the point
            y = upper_left_y + y_indices[i] * y_size + (y_size / 2)
            
            # DEBUG: coordinates
            #print (f"FOR X INDEX: {x_index},  Coords (x,y): {str(x)}, {str(y)}")
            #print(f"x_indices:{x_indices[i]}, x_size:{x_size}, upper_left_x:{upper_left_x}")
            #print(f"y_indices:{y_indices[i]}, y_size:{y_size}, upper_left_y:{upper_left_y}")

            point.SetPoint(0, x, y)
            Detections_feat.SetGeometry(point)
            Detections_layer.CreateFeature(Detections_feat)
            
            i += 1
            #print(f"A coordinate pair was created for each of the {i} pixel cells with value=255!")       # This line will slow down processing
    
    Detections_layer.SyncToDisk()
    
def Detectionmap(debug_mode, Paddock_path, epsg_textfile, tif_indir, Shapefile_path, Detections_path, Detectionmap_path):
# Code skeleton from https://gis.stackexchange.com/questions/42790/gdal-and-python-how-to-get-coordinates-for-all-cells-having-a-specific-value
# and https://stackoverflow.com/questions/36672753/methods-of-merging-shape_layers-shapefiles-using-ogr-in-python

    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Define Detections and point
    Detections_ds = driver.CreateDataSource(Detections_path)
    Detections_layer = Detections_ds.CreateLayer(Detections_path, geom_type = ogr.wkbPoint)
    Detections_def = Detections_layer.GetLayerDefn()
    Detections_feat = ogr.Feature(Detections_def)
    point = ogr.Geometry(ogr.wkbPoint)
    
    # Execute fast mode or debug mode
    if debug_mode == True:
        extract2shapefile(tif_indir, Shapefile_path, Detections_feat, Detections_layer, driver, point)
    else:
        extract2Detections(tif_indir, Detections_feat, Detections_layer, point)
    
    # Clip Detections inside paddock boundary to make Detectionmap
    try:
        print("Clipping Detections within paddock boundary...", end="")
        run(["ogr2ogr", "-clipsrc", Paddock_path, Detectionmap_path, Detections_path], check=True) #HOW TO USE OGR2OGR:   ogr2ogr -clipsrc clipper.shp output.shp clipped.shp
        print(" Done!")
    except:
        print("\n!!!!!!\nFILE OPEN IN ANOTHER PROGRAM (LIKELY QGIS), PLEASE CLOSE\n!!!!!!!")
        import sys
        sys.exit(0)
    
    # Give Detectionmap a spatial reference
    with open(epsg_textfile,'r') as epsg, open(Detectionmap_path.replace("shp", "prj"),'w') as Detectionmap_prj:
        for line in epsg:
            Detectionmap_prj.write(line)

    # Check spatial reference
    if path.exists(Detectionmap_path.replace("shp", "prj")) and path.getsize(Detectionmap_path.replace("shp", "prj")) > 0:
        print("Detectionmap projection file (.prj) exists and has content!")
    else:
        print("Detectionmap projection file (.prj) does not exist or has no content!")
        
    # Delete Detections
    Detections_ds.Destroy()
    driver.DeleteDataSource(Detections_path)

def Buffermap(buffer_metres, Paddock_path, epsg_textfile, tif_indir, Detectionmap_path, Buffermap_path):
# Code skeleton from http://pcjericks.github.io/py-gdalogr-cookbook/vector_layers.html#create-buffer

    # Open Detectionmap
    driver = ogr.GetDriverByName('ESRI Shapefile')
    Detectionmap_ds = ogr.Open(Detectionmap_path)
    try:
        Detectionmap_layer = Detectionmap_ds.GetLayer()
    except:
        print("\n!!!!!!\nFILE OPEN IN ANOTHER PROGRAM (LIKELY QGIS), PLEASE CLOSE\n!!!!!!!")
        import sys
        sys.exit(0)

    # Create Buffermap file
    Buffermap_ds = driver.CreateDataSource(Buffermap_path)
    Buffermap_layer = Buffermap_ds.CreateLayer(Buffermap_path, None, geom_type=ogr.wkbPolygon)
    Buffermap_def = Buffermap_layer.GetLayerDefn()

    print("Buffering Detectionmap...")
    # Buffer all Detectionmap points
    for feature in tqdm(Detectionmap_layer):
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(buffer_metres, 3)
        Buffermap_feat = ogr.Feature(Buffermap_def)
        Buffermap_feat.SetGeometry(geomBuffer)
        Buffermap_layer.CreateFeature(Buffermap_feat)
        Buffermap_feat = None

def Spraymap(Paddock_path, epsg_textfile, tif_indir, Buffermap_path, Dissolvemap_path, Spraymap_path):
# Code skeleton from https://gis.stackexchange.com/questions/296663/dissolve-not-based-on-attribute-in-geopandas
    
    # Check if there is a buffermap
    if not path.exists(Buffermap_path):
        print("\n\nThere is no Buffermap file to dissolve (it has probably been deleted by Spraymap())\nRun with only create_Buffermap = True and note that running create_Spraymap = True deletes Buffermap when finished with it\n")
        import sys
        sys.exit(0)

    # Read buffermap and add dissolve field
    print("Reading buffermap and adding dissolve field...", end="")
    Dissolvemap_gpd = gpd.read_file(Buffermap_path)
    Dissolvemap_gpd['dissolvefield'] = 1
    print(" Done!")
    
    # Dissolve overlapping polygons
    print("Dissolving polygons...", end="")
    Dissolvemap_inter = Dissolvemap_gpd.dissolve(by='dissolvefield') # This is type multipolygon
    Dissolvemap = gpd.geoseries.GeoSeries([geom for geom in Dissolvemap_inter.geometry.iloc[0].geoms]) # This is type polygon
    Dissolvemap.to_file(driver = 'ESRI Shapefile', filename = Dissolvemap_path)
    print(" Done!")

    # Clip Dissolvemap inside paddock boundary --> Spraymap
    try:
        print("Clipping spraymap within paddock boundary...", end="")
        run(["ogr2ogr", "-clipsrc", Paddock_path, Spraymap_path, Dissolvemap_path], check=True) #HOW TO USE OGR2OGR:   ogr2ogr -clipsrc clipper.shp output.shp clipped.shp
        print(" Done!")
    except:
        print("\n!!!!!!\nFILE LIKELY OPEN IN QGIS, PLEASE CLOSE\n!!!!!!!")
        import sys
        sys.exit(0)
    
    # Other (unsuccessful) methods for dissolving included:
    # --> geopandas in the same file as Spraymap.py using Spraymap_ds.flush() and os.fsync() to force write, but this is not possible for external programs, sleep() wont force write
    # --> QGIS API using a QgsVectorLayer layer object and passing it into processing.run native:dissolve then QgsVectorFileWriter.writeAsVectorFormat, but this requires running in the QGIS python terminal, which wasnt working as it would write 1kb large shapefiles
    # --> Command line ogr2ogr in subprocess.run(["", ""]) but sqlite wasnt working --> ogr2ogr F:\procdata\FW01\220112\procHR\C2\bitmap50x38_128x_Detectionmap\Spraymap_Dissolve_221219-113349.shp F:\procdata\FW01\220112\procHR\C2\bitmap50x38_128x_Detectionmap\Spraymap_221219-113349.shp -dialect sqlite -sql "SELECT ST_Union(geometry) FROM Spraymap_2212_19-113349"

    # Give Spraymap a spatial reference
    with open(epsg_textfile,'r') as epsg, open(Spraymap_path.replace("shp", "prj"),'w') as Spraymap_prj:
        for line in epsg:
            Spraymap_prj.write(line)

    # Check spatial reference
    if path.exists(Spraymap_path.replace("shp", "prj")) and path.getsize(Spraymap_path.replace("shp", "prj")) > 0:
        print("Spraymap projection file (.prj) exists and has content!")
    else:
        print("Spraymap projection file (.prj) does not exist or has no content!")

    # Delete Buffermap
    if path.exists(Buffermap_path):
        remove(Buffermap_path)
        remove(Buffermap_path.replace("shp","shx"))
        remove(Buffermap_path.replace("shp","dbf"))
        print("Deleted Buffermap!")
        
    # Delete Dissolvemap
    if path.exists(Dissolvemap_path):
        remove(Dissolvemap_path)
        remove(Dissolvemap_path.replace("shp","shx"))
        remove(Dissolvemap_path.replace("shp","dbf"))
        if path.exists(Dissolvemap_path.replace("shp","cpg")):
            remove(Dissolvemap_path.replace("shp","cpg"))
        print("Deleted Dissolvemap!")

def Tractormap(Paddock_path):
    import pandas as pd
    #import fsspec
	
    #1. *Merge* the three camera spraymaps into one layer using unary_union shapely/geopandas(shapely) which also DISSOL:VES
    
    
    #with fsspec.open('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C2.shp') as spraymap:
    df1 = gpd.read_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C1.shp')
    df2 = gpd.read_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C2.shp')
    df3 = gpd.read_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C3.shp')
    df4 = pd.concat([df1.geometry, df2.geometry, df3.geometry])
    gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(df4))
    df6 = gdf.dissolve() #df4.unary_union
    #df6 = gpd.geoseries.GeoSeries([geom for geom in df5.geometry.iloc[0].geoms])
    #df6 = gpd.GeoSeries(df5).explode(index_parts=False, geom_type=geoseries)
    #df6 = gpd.GeoDataFrame(df5).explode(index_parts=False, geom_type=geoseries)
    df6["CHEMICALVO"]= 100.0
    #AREA ---> df6['geometry'].to_crs({'proj':'cea'}).map(lambda p: p.area / 10**6)    To get correct area, you must use 'equal-area' projection. The one that works well with your code is epsg 6933. It is cylindrical equal-area projection.
    print('!!!!!')
    print(df6.area)  # but this works (spits out 4 items)
    print('!!!!!')
    df6.to_file("D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_UU_Chem0.shp")
    
    
    #### Dissolvemap_inter = Dissolvemap_gpd.dissolve(by='dissolvefield') # This is type multipolygon
    ####Dissolvemap = gpd.geoseries.GeoSeries([geom for geom in Dissolvemap_inter.geometry.iloc[0].geoms]) # This is type polygon
    
   # with gpd.read_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C2_Barney.shp') as spraymap:
   #     spraymap.unary_union()
   #     spraymap.to_file("D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C2_Barney_UU.shp")
    
    #from shapely.ops import unary_union
    #import fiona
    #with fiona.open('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Spraymap_999999_C2_Barney.shp') as spraymap:
        #print(len(list(spraymap)))
        #unary_union(spraymap)
    
    # OR USE TO MERGE
        #ogrmerge.py -single -o merged.shp france.shp germany.shp       #FROM https://gdal.org/programs/ogrmerge.html

    #2.*Dissolve* them
        # covered by unary union
    
	#3. Add field ‘CHEMICALVO=100’, type Decimal Number, precision=2. https://gis.stackexchange.com/questions/215963/adding-column-to-shapefile-with-fiona
    #4. Use the same process as last step to add field ‘CHEMICALVO=0’ (zero) to the paddock layer.       
    paddock = gpd.read_file(Paddock_path)
    paddock["CHEMICALVO"]= 0.0
    paddock_gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(paddock.geometry))
    paddock_gdf.to_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\Paddock_999999_Chem0.shp')

	#5. *Difference* using shapely to cut holes in the paddock where the CHEMICALVO=100 polygons will go.
    # https://geopandas.org/en/stable/docs/reference/api/geopandas.GeoSeries.difference.html
        # a=dissolved_100
        # b=paddock_0
        # b.difference(a)
    holes = paddock_gdf.geometry.difference(df6, align=False) #"We can either align both GeoSeries based on index values and compare elements with the same index using align=True or ignore index and compare elements based on their matching order using align=False"
    holes.to_file('D:\procdata\FW02\999999\F2\procHR\maps\Spraymap\holes.shp')

    
	#6. merge the dissolved spraymap and the difference'd holes layer
        # as in first step, use ogrmerge or shapely unary_union

    #7. delete unwanted attribute fields by grabbing only the ones you want and creating a new .dbf file (then copy over the other files .shp .shx .prj)
        # ogr2ogr -f “ESRI Shapefile” -select attribute1, attribute2 output.shp input.shp
        # Alternatively, with sql from the original blog https://blog.rtwilson.com/quick-way-to-delete-columns-from-a-shapefile/
        # ogr2ogr -f "ESRI Shapefile" -sql "SELECT attribute1, attribute2 FROM input" output.shp input.shp
        # MIGHT BE ABLE TO DO WITH GPD similar to adding
   
    #8. REPROJECT from 32755 to epsg 4320 wgs84
        #ogr2ogr -f "ESRI Shapefile" -t_srs EPSG:NEW_EPSG_NUMBER -s_srs EPSG:OLD_EPSG_NUMBER output.shp input.shp       #FROM https://stackoverflow.com/questions/44515255/how-to-project-the-shapefile-with-gdal-ogr2ogr
            
    # or from https://gis.stackexchange.com/questions/375407/geopandas-intersects-doesnt-find-any-intersection
        # regions=regions.to_crs('epsg:4326')
        # points=points.set_crs('epsg:4326')
    


def Reproject():
    

    # this function may not be needed at all since tractormap may reproject into 4320 :)
    
    print('There is nothing in Reproject yet!')

def main():
    
    t0 = perf_counter()
    cameras = setParams()[0]
    cameras_string = [str(x) for x in cameras]
    for camera in cameras_string:
        cameras, map_suffix, create_Detectionmap, create_Buffermap, create_Spraymap, create_Tractormap, debug_mode, buffer_metres, project, date, flight, Paddock_path, epsg_textfile = setParams()

        #tif_indir = r'D:\procdata\FW02\230118\F2\procHR\camera\geotiff_bmp'
        tif_indir = 'D:\\procdata' +'\\'+ project +'\\'+ date +'\\'+ flight +'\\procHR\\C'+ camera +'\\geotiff_bmp' # Where is the source directory for the input bitmap .tifs?
        
        Parent_outdir = 'D:\\procdata' +'\\'+ project +'\\'+ date +'\\'+ flight +'\\procHR\\maps\\'
        
        Shapefile_path = Parent_outdir      + 'Intermediary_Shapefiles\\'
        Detections_path = Parent_outdir     + 'Detectionmap\\Detections_'       + date + '_C' + camera + '_' + map_suffix + '.shp'
        Detectionmap_path = Parent_outdir   + 'Detectionmap\\Detectionmap_'     + date + '_C' + camera + '_' + map_suffix + '.shp'
        Buffermap_path = Parent_outdir      + 'Spraymap\\Buffermap_'            + date + '_C' + camera + '_' + map_suffix + '.shp'
        Dissolvemap_path = Parent_outdir    + 'Spraymap\\Dissolvemap_'          + date + '_C' + camera + '_' + map_suffix + '.shp'
        Spraymap_path = Parent_outdir       + 'Spraymap\\Spraymap_'             + date + '_C' + camera + '_' + map_suffix + '.shp'
        
        print("\n--------------------------------------------")
        print(f"----------------- CAMERA {camera} -----------------")
        print("--------------------------------------------")
     
        if create_Detectionmap is True:
            print(f"\n-------- Camera {camera} Detectionmap ---------")
            Detectionmap(debug_mode, Paddock_path, epsg_textfile, tif_indir, Shapefile_path, Detections_path, Detectionmap_path)
            
        if create_Buffermap is True:
            print(f"\n-------- Camera {camera} Buffermap ---------")
            Buffermap(buffer_metres, Paddock_path, epsg_textfile, tif_indir, Detectionmap_path, Buffermap_path)
            
        if create_Spraymap is True:
            print(f"\n-------- Camera {camera} Spraymap ---------")
            Spraymap(Paddock_path, epsg_textfile, tif_indir, Buffermap_path, Dissolvemap_path, Spraymap_path)
            
        if create_Tractormap is True:
            print(f"\n-------- Camera {camera} Tractormap ---------")
            Tractormap(Paddock_path)
            #Reproject()
            
    t1 = perf_counter()
    print(f"\nTime to complete script was {round(t1-t0,2)} seconds")
    

if __name__ == '__main__':
    main()
    exit()
