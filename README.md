# Welcome to Skyscan PostFlight Processing
by Evan Boutsalis

## Purpose
To georeference high res imagery and weed detection bitmaps.
Georeferenced high res imagery is used for quality control.
Georeferenced weed detection bitmaps are placed into a processing pipeline which uses the pixel coordinates for individual weeds to tell a tractor where to spray using the ESRI shapefile format.
Additional insights section provides information on weed density and spray coverage.

## Dependencies
- Image extraction
	- hrimgexport_basetest.exe
	- hrdebayer.exe
- Georeferencing
	- pprep
	- pixref
	- bsqgrid
- Shapefile creation, manipulation, analysis
	- python3, be sure to tick "add to PATH" during install wizard
	- python libraries: gdal, ogr, and geopandas
	- QGIS

## Required files
- Sequence files 	 (.seq)
- Sequence csv for each camera 	 (.csv)
- Processed nav data (.csv, to continue past step 2.3)
- Lens model 		 (.bsq)
- Surface model 	 (.obj, otherwise define a single-altitude surface)

## Installation
Pull down the master branch. 
- If *using* the scripts, first copy repository directory content into relevant project directory, such as D:\projects\FW02\proc\procHRcam.
- If *developing*, edit the files within the repository directory. Edit and use **CopyToProjects.bat** to sync and run scripts in project directory.
*Keep files in the same directory.*

## Sprayrig shapefile procedure
**Edit scripts in C:\Skyscan-PostFlightDev and then run C:\Skyscan-PostFlightDev\CopyToProjects.bat before executing the scripts in D:\projects\FW02\proc\procHRcam\\**

1. Extract the high res imagery directly from the three sequence drives from the plane and debayer it
	- Edit **Imagery_ExportRenameDebayer_fromSeqDrives.py** by going to the function PARAMETERS() and changing them to suit
	- Run > python **Imagery_ExportRenameDebayer_fromSeqDrives.py**
2. Create matched Nav file for use in pprep scripts (+ a flight track kml)
	- Edit **Nav_CombineMetadataCsv.ps1** accounting for project, date, flight
	- Run **Nav_CombineMetadataCsv.ps1** from powershell or right-click ‘Run with Powershell’
	- When processed Nav data has been returned, match with combined sequence file .csv's:
		- Edit **Nav_CreateMatched.bat** accounting for flight, date, and Nav filename
		- Run **Nav_CreateMatched.bat**
3. Georeference imagery
	- Edit **HRrectifyImagery.ppr** variables: RefSurface, UTMZone, InDir, OutDir, NavFile, LensModel
	- Run in project directory --> ppr32 .\\**HRrectifyImagery** (leave off file extension .ppr)
	- Load images into Global Mapper 12 to check the following:
		- Images along the plane track are contiguous
		- Same Frame# from cameras 1,2,3 lie perpendicular to the plane track	
		- Load world imagery underneath looking for similarities
4. Georeference weed detection bitmaps
	- Edit **HRrectifyBitmap.ppr** variables: RefSurface, UTMZone, InDir, OutDir, NavFile, LensModel
		- {If you would like to georeference and process a large batch of data in one go, skip to step 5}
	- Run in project directory --> ppr32 .\\**HRrectifyBitmap** (leave off file extension .ppr)
	- Load images into Global Mapper 12 to check they overlap with imagery
5. Execute the shapefile processing pipeline 
	- Edit **Skyscan_Config.py** and define every variable in setParams()
		- {If you would like to batch process as mentioned in step 4, skip the next step and instead run **Rectify+ShapefileGen.bat**}
	- Run **Skyscan_ShapefileGen.py** to process shapefiles for all 3 cameras
6. Create Tractormaps (shapefiles ready for tractor spraying)
	- Open the three camera spraymap shapefiles and the paddock shapefile in QGIS
	- *Merge* the three camera spraymaps into one layer using Vector > Data management tools>Merge Vector Layers
	- *Dissolve* the merged layer using Vector > Geoprocessing Tools > Dissolve (no dissolve field specified)
	- Remove the (now redundant) merged layer from the layer list
	- Add fields:
		- Select the dissolved spraymap in layer list > right click> Open attribute table> toggle editing mode (yellow pencil)
		- Click the button with the yellow column (add field) > field:‘CHEMICALVO’, type: Decimal Number, precision: 2
		- Fill in the cell with 100
		- Toggle editing off and Save
		- Select the paddock layer in layer list > right click> Open attribute table> toggle editing mode (yellow pencil)
		- Click the button with the yellow column (add field) > field:‘CHEMICALVO’, type: Decimal Number, precision: 2
		- Fill in the cell with 0
		- Toggle editing off and Save
	- Use the shapefile *Difference* tool Vector > Geoprocessing Tools > Difference (input: paddock, overlay: merged+dissolved) to cut holes in the paddock where the CHEMICALVO=100 polygons will go
	- Perform Vector > Data Management Tools > Merge Vector Layers on the dissolved spraymap and the difference'd paddock layer with holes
	- In the new merged layer attribute table, enable editing (yellow pencil), then column delete button (red column with 'x') and remove all attribute fields except CHEMICALVO
	- Right-click layer > Export > Save Features As > \[paddock_date_bufferradius\] > CRS: EPSG 4326 / WGS84
	- Sip lemonade by the pool 🥃🏊

## Additional insights
### Derive estimate factor for use in quickly estimating weed density prior to processing (detection:spray), requires having completed Sprayrig shapefile procedure up to step 4. Georeference weed detection bitmaps
1. Create data subset
	- Observe flightlines from nav file kml in Global Mapper 12 or Google Earth Pro --> D:\procdata\FW02\230118\F2\procHR\HR_230118_F2_C2_matched.kml
	- List out the sequence and frame numbers which correspond to a trial site of interest e.g. from top->bottom of paddock Broncos 230118
		- track,seq,first,last
		- 1,04,044,161
		- 2,05,064,188
		- 3,06,057,161
		- 4,07,055,170
		- 5,08,052,147
	- Edit **Subset_SeqFrames.py**: Sequences, FirstFrames, LastFrames, indir, outdir
    - run > python **Subset_SeqFrames.py**
2. Count average detection points per frame
	- edit **CountBitmapDetections.py**, changing only input directory (created by Subset_SeqFrames.py in step 1)
	- run > python **CountBitmapDetections.py**
3. Copy the corresponding georeferenced bitmaps into a subset folder 
	- edit **Subset_SeqFrames.py**: changing only the indir, outdir, and filename (be mindful of file extension) (the following should stay the same: Sequences, FirstFrames, LastFrames)
	- run > python **Subset_SeqFrames.py**
	- Alternatively, georeference your raw subset folders.
4. Execute the shapefile processing pipeline as per step 5. in *Sprayrig shapefile procedure* for the georeferenced subset
5. \[QGIS\] Calculate % Spray Coverage (SprayArea/PaddockArea = SprayCoverage%)
	- Open the three camera spraymap shapefiles in QGIS.
	- *Merge* the three camera spraymaps into one layer using Vector>Data management tools>Merge Vector Layers and *Dissolve* them using Vector->Geoprocessing Tools>Dissolve (no dissolve field specified).
	- Paddock(s) area
		- Open the paddock shapefile: FW22_CultivationPaddocks.shp
		- Select paddock layer in Layer List, select paddock(s) using select tool, then ctrl+C, then ctrl+Alt+V. Alternatively (if you like using the mouse) Edit>Copy Features then Edit>Paste Features as> Temporary Scratch Layer
		- Paddock area is listed in hectares in attribute table under field 'ha' (NOT 'area', which is a string)
	- Spray area within paddock(s)
		- *Intersect* using Vector>Geoprocessing Tools>Intersection with input layer paddock and overlay layer merged+dissolved spraymap
		- Check Project>Properties>General>Area is set to Square Metres
		- Enter the attribute table of the intersected spraymap
		- Toggle editing, open Field Calculator (Abacus icon)
		- Tick 'Create new a field', Output field name: sprayarea_ha, Output field type: Decimal Number (real), Expression: $area/10000
6. Estimate factor is: 
	- Spray coverage = spray hectares/ Broncos hectares = 35.758/170 = 21%
	- The total average number of detections was: 14.7
	


### Estimate weed density prior to processing (detection:spray)
1. Count average detection points per frame
	- edit **CountBitmapDetections.py**, changing only input directory (subset directory if required per paddock)
	- run **CountBitmapDetections.py**
2. Multiply the total average number of detections by the derived estimate factor.

### Calculate accurate weed density post-processing
- *potential solutions...*
	- Vector>Analysis Tools>Count Points in Polygon
	- Rightclick on weedmap layer -> Properties... -> Symbology -> and choose "Point cluster"
	- Processing Toolbox>Vector analysis>DBSCAN clustering
	
### Spatial test polygon for sprayrig accuracy
- Edit tool > Add polygon, using a land feature on google earth imagery. When finished drawing, set Spray=0 (so the sprayrig wont activate).

## Resources
[Skyscan](https://skyscan.com.au/)

[Airborne Research Australia](https://www.airborneresearch.org.au/)

[GDAL/OGR Cookbook](https://pcjericks.github.io/py-gdalogr-cookbook/)

[Geopandas Docs](https://geopandas.org/en/stable/docs.html)

[QGIS](https://qgis.org/en/site/)

[ESRI Shapefile - Technical Description pdf](https://www.esri.com/content/dam/esrisites/sitecore-archive/Files/Pdfs/library/whitepapers/pdfs/shapefile.pdf)

## File trees on Skyscan Workstation
```
D:
    ├───procdata
    │   └───FW02
    │       ├───230118
    │       │   ├───F1
    │       │   │   ├───proc6D
    │       │   │   ├───procHR
    │       │   │   │   ├───C1
    │       │   │   │   │   ├───geotiff_bmp
    │       │   │   │   │   └───geotiff_jpg
    │       │   │   │   ├───C2
    │       │   │   │   │   ├───geotiff_bmp
    │       │   │   │   │   └───geotiff_jpg
    │       │   │   │   ├───C3
    │       │   │   │   │   ├───geotiff_bmp
    │       │   │   │   │   └───geotiff_jpg
    │       │   │   │   └───maps
    │       │   │   │       ├───Detectionmap
    │       │   │   │       ├───Intermediary_Shapefiles
    │       │   │   │       ├───Spraymap
    │       │   │   │       └───Tractormap
    │       │   │   └───procNav
    │       │   └───F2
    │       │       ├───proc6D
    │       │       ├───procHR
    │       │       │   ├───C1
    │       │       │   │   ├───geotiff_bmp
    │       │       │   │   └───geotiff_jpg
    │       │       │   ├───C2
    │       │       │   │   ├───geotiff_bmp
    │       │       │   │   └───geotiff_jpg
    │       │       │   ├───C3
    │       │       │   │   ├───geotiff_bmp
    │       │       │   │   └───geotiff_jpg
    │       │       │   └───maps
    │       │       │       ├───Detectionmap
    │       │       │       ├───Intermediary_Shapefiles
    │       │       │       ├───Spraymap
    │       │       │       └───Tractormap
    │       │       └───procNav
    │       ├───DEM
    │       └───Paddock
    ├───proctemp
    └───projects
        └───FW02
            └───proc
                ├───cfg
                ├───procHRcam
                │   ├───RetiredScripts
                │   └───temp
                └───tmp
```
```
E:
└───rawdata
	└───FW02
	    └───230118
	        ├───F1
	        │   ├───raw6D
	        │   ├───rawHR
	        │   │   ├───C1
	        │   │   │   ├───bitmap
	        │   │   │   ├───jpg
	        │   │   │   └───raw
	        │   │   ├───C2
	        │   │   │   ├───bitmap
	        │   │   │   ├───jpg
	        │   │   │   └───raw
	        │   │   └───C3
	        │   │       ├───bitmap
	        │   │       ├───jpg
	        │   │       └───raw
	        │   └───rawNav
	        └───F2
	            ├───raw6D
	            ├───rawHR
	            │   ├───C1
	            │   │   ├───bitmap
	            │   │   ├───jpg
	            │   │   └───raw
	            │   ├───C2
	            │   │   ├───bitmap
	            │   │   ├───jpg
	            │   │   └───raw
	            │   └───C3
	            │       ├───bitmap
	            │       ├───jpg
	            │       └───raw
	            └───rawNav
```
