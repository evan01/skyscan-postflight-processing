
# This script is used to subset raw imagery or bitmaps based on what is desired from the flight track

# 1. Specify Sequence files and frame ranges
# 2. Change input and output directory
# 3. run > python Subset.py

from os import path, listdir, fsencode, fsdecode, mkdir
import shutil

#       track,seq,first,last
#		- 1,04,044,161
#		- 2,05,064,188
#		- 3,06,057,161
#		- 4,07,055,170
#		- 5,08,052,147

Sequences = [9,10,11,12,15,16,17,20,21]
FirstFrames = [40,40,55,23,45,72,70,61,27]
LastFrames = [155,128,153,90,126,123,126,115,68]

i = 0
for camera in ['1','2','3']:

    #indir = r'E:\rawdata\FW02\230118\F2\rawHR\C'+ camera +r'\bitmap'                #RAW
    indir = r'D:\procdata\FW02\230118\F2\procHR\C'+ camera +r'\geotiff_bmp'         #GEOREF
    #outdir = r'E:\rawdata\FW02\230118\F2\rawHR\C'+ camera +r'\bitmap_subset_Silo'   #RAW
    outdir = r'D:\procdata\FW02\230118\F2\procHR\C'+ camera +r'\geotiff_bmp_subset_Silo'   #GEOREF

    
    indir_encode = fsencode(indir)
    outdir_encode = fsencode(outdir)
    
    if not path.exists(indir_encode):
        print(f"There is no directory: {indir_encode}")
        print("Terminating...")
        import sys
        sys.exit(0)
    if not path.exists(outdir_encode):
        mkdir(outdir)
    
    i=0
    for seq in Sequences:
        seq3 = str(Sequences[i]).zfill(3)
        for frame in range(FirstFrames[i],LastFrames[i]):
            frame4 = str(frame).zfill(4)
            filename = 'HR_230118_F2_C'+ camera +'_'+ seq3 +'_'+ frame4 + '.tif'            #!!!!!!!!!!!!!
            print(f"Moving file {filename}")
            shutil.move(path.join(indir,filename),path.join(outdir,filename))
        i += 1