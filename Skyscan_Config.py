# PURPOSE: of this file is to specify all parameters for ShapefileGenerator.py
#   --> Detectionmap() (creates a combined shapefile Detectionmap of every .tif image in a directory, trimmed within a paddock boundary)
#   --> Buffermap() (buffers every point in a Detectionmap then clips the buffer polygons within a paddock boundary to produce spraymap with overlapping polygons)
#   --> Spraymap() (dissolves polygons in spraymap then clips inside paddock boundary, needed separate from Buffermap.py because python wont write to Buffermap until Buffermap.py ends and geopandas was therefore reading in an empty geodataframe)

# WARNING: Scripts may encounter issues if an included layer is open in another program, such as ERROR 1: (output filename) is not a directory.

# HOW TO USE this script:
#   1. Install python3, be sure to tick "add to PATH" during install wizard
#   2. Install python libraries gdal, ogr, and geopandas using $ pip install {package} >>> when I did this on ARA laptop I had to maually install gdal using a .wheel file
#   3. Keep this include file in the same directory as ShapefileGenerator.py
#   4. Specify all of the parameters in SetParams below
#   5. Create any directories you specified which do not already exist (xcopy "x" "y" /T /F)
#   6. Open a powershell instance in the folder with the scripts by going File > Open Windows Powershell
#   7. Run python .\ShapefileGenerator.py
#   8. To get in tractor ready format:
#           open the three camera spraymap shapefiles in QGIS
#           merge the three camera spraymaps into one layer and dissolve them
#           add CHEMICALVO=100 double field (precision=2) to spraymap and CHEMICALVO=0 field to paddock using attribute table editing mode
#           perform shapefile difference tool to cut holes in paddock where CHEMICALVO=100 polygons will go
#           perform merge vector layers in data management to combine the 100 and 0 values into one shapefile
#           Delete all other attribute fields
#           export to EPSG 4326

# Possible IMPROVEMENTS include: 
#   --> create a script which does the QGIS part of the methodology and Reproject.py file 
#   --> create single Hrrectify for imagery a bitmap using $IF imagetype == bitmap then names have {imagetype}
#   --> To speed up trimming within boundary: Detect which paddocks have points inside and use only those polygons in the trim

def setParams():

    # INFO
    project = 'FW02'          # for building directory names, so keep consistent with directory structure
    date = '230219'           # for building directory names, so keep consistent with directory structure
    flight = 'F1'             # for building directory names, so keep consistent with directory structure
    cameras = [1,2,3]
    
    # OPTIONS
    map_suffix = 'BrosnansWest_5m'                   # Specify what to end the output filenames with (not the extension) eg. Spraymap_230118_C2_{map_suffix}.shp
    create_Detectionmap = False              # True or False
    create_Buffermap = False                 # Requires 'create_Detectionmap = True', or a Detectionmap already generated
    create_Spraymap = False                  # Requires 'create_Detectionmap = True' and 'create_Buffermap = True', or a Detectionmap and buffermap already generated
    create_Tractormap = True               # ^^ and so on...
    debug_mode = False                      # Debug mode will create 1 shapefile per tif saved to Shape_outdir but is considerably slower
    buffer_metres = 5.0

    # FILE LOCATIONS
    #Paddock_path = r'D:\procdata\FW02\Paddock\FW22_TwinTanks.shp'   # Where is the paddock boundary shapefile?
    #Paddock_path = r'D:\procdata\FW02\Paddock\FW22_Brosnans_West.shp'   # Where is the paddock boundary shapefile?
    Paddock_path = r'D:\procdata\FW02\Paddock\Paddock_999999.shp'   # Where is the paddock boundary shapefile?
    epsg_textfile = r'D:\projects\FW02\proc\procHRcam\EPSG32755.txt'          # Writes shapefile .prj content using text file. To create a new text file copy+paste contents from site https://epsg.io/32755 using 'ESRI WKT' tab

    # Dont touch
    return cameras, map_suffix, create_Detectionmap, create_Buffermap, create_Spraymap, create_Tractormap, debug_mode, buffer_metres, project, date, flight, Paddock_path, epsg_textfile