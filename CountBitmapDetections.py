from os import remove, path, listdir, fsdecode, fsencode
from time import perf_counter
from osgeo import gdal, ogr
from numpy import nonzero
from tqdm import tqdm


t0 = perf_counter()
totallist = []
for camera in ['1','2','3']:
    #bmp_indir = r'E:\rawdata\FW02\230118\F2\rawHR\C2\bitmap'
    #E:\rawdata\FW02\230118\F2\rawHR\C2\bitmap_subset_Broncos
    #D:\procdata\FW02\230118\F2\procHR\C2\geotiff_bmp
    
    
    bmp_indir = r'E:\rawdata\FW02\230218\F1\rawHR\C'+ camera +r'\bitmap_old'                   # RAW
    #bmp_indir = r'D:\procdata\FW02\230118\F2\procHR\C'+ camera +r'\geotiff_bmp'             # PROC
    
    bmp_indir_encode = fsencode(bmp_indir)
    
    if not path.exists(bmp_indir_encode):
        print(f"There is no directory: {bmp_indir_encode}")
        print("Terminating...")
        import sys
        sys.exit(0)
    
    print("\nCamera",camera, bmp_indir)
    
    cameralist = []
    for bmp in tqdm(listdir(bmp_indir_encode)):
        
        # Open only bmps
        bmp_decode = fsdecode(bmp)
        if not bmp_decode.endswith('.bmp'): #and bmp_decode.endswith('.tif'):
            continue

        # Read raster, band1, and create array from band1
        raster = gdal.Open(bmp_indir + "\\" + bmp_decode)
        band1 = raster.GetRasterBand(1)
        (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = raster.GetGeoTransform()
        array = band1.ReadAsArray().astype(int)
        
        # This evaluation makes a numpy x/y index array containing every pixel cell with the fixed value, 255.
        (y_indices, x_indices) = nonzero(array == 255)
        points = len(x_indices)
        cameralist.append(points)
        totallist.append(points)
    print(f"Camera average number of detections is {round(sum(cameralist)/len(cameralist),1)}")

t1 = perf_counter()
print(f"\nTime to complete was {round(t1-t0,2)} seconds")
print(f"The total average number of detections was: {round(sum(totallist)/len(totallist),1)}")