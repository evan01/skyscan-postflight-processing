:: Create a matched nav file for each camera
.\hr_match_ARA_metadata.exe D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C1.csv D:\procdata\FW02\230219\F1\procNav\230219_010553_xNav14_ndiff_trig1.csv D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C1_matched.csv
.\hr_match_ARA_metadata.exe D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C2.csv D:\procdata\FW02\230219\F1\procNav\230219_010553_xNav14_ndiff_trig1.csv D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C2_matched.csv
.\hr_match_ARA_metadata.exe D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C3.csv D:\procdata\FW02\230219\F1\procNav\230219_010553_xNav14_ndiff_trig1.csv D:\procdata\FW02\230219\F1\procHR\HR_230219_F1_C3_matched.csv


:: Create a kml flight track (should have same name as procNav above, but with _100 instead of _Trig, LEAVE OFF .csv)
nav2kml E:\rawdata\FW02\230219\F1\rawNav\230219_010553_xNav14_ndiff_100

:: Move the flight track from rawNav to procNav
move E:\rawdata\FW02\230219\F1\rawNav\230219_010553_xNav14_ndiff_100.kml D:\procdata\FW02\230219\F1\procNav\230219_010553_xNav14_ndiff_100.kml